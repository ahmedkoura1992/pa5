#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
/*
 @author:Ahmed Aboukoura.
this a one class that takes the input in the form of memory location like that for example 0x804ae19: 0x804ae19: R 0x9cb3d40
0x804ae19: W 0x9cb3d40
0x804ae1c: R 0x9cb3d44
0x804ae1c: W 0x9cb3d44
0x804ae10: R 0xbf8ef498
#eof
 and then output the number of memory, reads, memory hits, cache hits, cache misses, like that for example 
 Prefetch 0
Memory reads: 168
Memory writes: 334
Cache hits: 832
Cache misses: 168
Prefetch 1
--------------
please check this documentation to know what we i'm doing in here.
https://content.sakai.rutgers.edu/access/content/attachment/dd7c916d-5bfb-4eca-bdbd-471a8862a392/Assignments/dc513045-31bb-4b49-84d3-21ec1a5a3798/pa5.pdf
*/


int logtwo(int x ){
float f = (float)x;
float c = log2f (f);
int my_var = (int)c;
return my_var;
}
int read;
int write;
int hit;
int miss;

int read2;
int write2;
int hit2;
int miss2;
int isItPowerTwo(int n)
{
   if (n == 0){
      return 0;
      }
   while( n != 1)
   {
      if(n % 2 != 0)
         return 0;
         n = n / 2;
   }
   return 1;
}

void mymainmethod(int argc, char **argv) {

typedef struct CashLine{
int valid;
unsigned long int tag;

unsigned long int age;

}CashLine;
struct CashLine** cache;

if (argc != 6) {
printf("wrong number of inputs\n");
}
    int cacheSize = atoi(argv[1]);
    if(isItPowerTwo(cacheSize) != 1){
        printf("the cashe size is not a power of 2\n");
    }
    char* associativity = argv[2];
    int blockSize = atoi(argv[4]);
       // int cacheSize = atoi(argv[1]);
    if(isItPowerTwo(blockSize) != 1){
        printf("the blocksize is not a power of 2\n");
    }
    FILE *file1 = fopen(argv[5],"r");
         if(associativity[0] == 'd'){

        int setNumbers = cacheSize/blockSize;
        int index_size = logtwo(setNumbers);
        int offset_size = logtwo(blockSize);
        cache=(CashLine**)malloc(setNumbers*sizeof(CashLine*));
      for(int i=0;i<setNumbers;i++){
                cache[i]=(CashLine*)malloc((1)*sizeof(CashLine));
        }
        for(int i=0;i<setNumbers;i++){

            cache[i][0].valid=0;
            cache[i][0].age=0;
            cache[i][0].tag=0;
      }
      int age = 0;

while (!feof(file1)) {
char pc[99];
char input;
unsigned long int address;
unsigned long int tag;
unsigned long int set_index;

fscanf(file1,"%s %c %lx", pc, &input, &address);
//reads file and sets address
if (pc[0] == '#') {break;}

         long int identification = address >> offset_size;
              set_index = identification & ((1 << index_size) - 1);
               tag = identification >> index_size;

if (input == 'R'){
   if ((cache[set_index][0].valid == 1)&&(cache[set_index][0].tag == tag)) {

hit++;

}else{
cache[set_index][0].valid = 1;
cache[set_index][0].tag = tag;
cache[set_index][0].age = age;
read++; miss++;
}
}else if(input == 'W'){
    if ((cache[set_index][0].valid == 1)&&(cache[set_index][0].tag == tag)) {
write++; hit++;


}else{
cache[set_index][0].valid = 1;
cache[set_index][0].tag = tag;
cache[set_index][0].age = age;
read++; write++; miss++;
}
}
age++;
}
printf("Memory reads: %d\nMemory writes: %d\nCache hits: %d\nCache misses: %d\n",read, write,hit,miss);
}else if(associativity[5] == ':'){
        char c = associativity[5];
        int n = c - '0';
        int setNumbers = cacheSize/blockSize;
        int index_size = logtwo(setNumbers);
        int offset_size = logtwo(blockSize);
        cache=(CashLine**)malloc(setNumbers*sizeof(CashLine*));
      for(int i=0;i<setNumbers;i++){
                cache[i]=(CashLine*)malloc((n)*sizeof(CashLine));
        }
        for(int i=0;i<setNumbers;i++){
         for(int k =0;k<n;k++){
            cache[i][k].valid=0;
            cache[i][k].age=0;
            cache[i][k].tag=0;
         }
      }
      int age = 0;

while (!feof(file1)) {
   char pc[99];
char input;
unsigned long int address;
unsigned long int tag;
unsigned long int set_index;
fscanf(file1,"%s %c %lx", pc, &input, &address);
if (pc[0] == '#') {break;}
         long int identification = address >> offset_size;
              set_index = identification & ((1 << index_size) - 1);
               tag = identification >> index_size;



if (input == 'R'){
          /////////////
              int k = 0;
for(int l =0;l<n;l++){
   if(cache[set_index][k].valid != 0 && cache[set_index][k].tag != tag && k<n){
       k = l;
   }
}
            //////////////

   if ((cache[set_index][k].valid == 1)&&(cache[set_index][k].tag == tag)) {

hit++;

}else{
    int min = 0;
k = 0;
 for(int l =0;l<n;l++){
   if(cache[set_index][k].age < cache[0][min].age){
       min = k;
   }
}
cache[set_index][min].valid = 1;
cache[set_index][min].tag = tag;
cache[set_index][min].age = age;
read++; miss++;
}
}else if(input == 'W'){
                int k = 0;
for(int l =0;l<n;l++){
   if(cache[set_index][k].valid != 0 && cache[set_index][k].tag != tag && k<n){
       k = l;
   }
}
            //////////////
    if ((cache[set_index][0].valid == 1)&&(cache[set_index][0].tag == tag)) {
write++; hit++;


}else{
        int min = 0;
k = 0;
 for(int l =0;l<n;l++){
   if(cache[set_index][k].age < cache[0][min].age){
       min = k;
   }
}
cache[set_index][min].valid = 1;
cache[set_index][min].tag = tag;
cache[set_index][min].age = age;
read++; write++; miss++;

}
}
age++;
}
printf("Memory reads: %d\nMemory writes: %d\nCache hits: %d\nCache misses: %d\n",read, write,hit,miss);
}else {

        int n =  cacheSize/blockSize;
        int setNumbers = 1;
        int index_size = logtwo(setNumbers);
        int offset_size = logtwo(blockSize);
        cache=(CashLine**)malloc(setNumbers*sizeof(CashLine*));
      for(int i=0;i<setNumbers;i++){
                cache[i]=(CashLine*)malloc((n)*sizeof(CashLine));
        }
        for(int i=0;i<setNumbers;i++){
         for(int k =0;k<n;k++){
            cache[i][k].valid=0;
            cache[i][k].age=0;
            cache[i][k].tag=0;
         }
      }
      int age = 0;

while (!feof(file1)) {
   char pc[99];
char input;
unsigned long int address;
unsigned long int tag;
unsigned long int set_index;
fscanf(file1,"%s %c %lx", pc, &input, &address);
if (pc[0] == '#') {break;}
         long int identification = address >> offset_size;
              set_index = identification & ((1 << index_size) - 1);
               tag = identification >> index_size;



if (input == 'R'){
          /////////////
              int k = 0;
for(int l =0;l<n;l++){
   if(cache[set_index][k].valid != 0 && cache[set_index][k].tag != tag && k<n){
       k = l;
   }
}
            //////////////

   if ((cache[set_index][k].valid == 1)&&(cache[set_index][k].tag == tag)) {

hit++;

}else{
    int min = 0;
k = 0;
 for(int l =0;l<n;l++){
   if(cache[set_index][k].age < cache[set_index][min].age){
       min = k;
   }
}
cache[set_index][min].valid = 1;
cache[set_index][min].tag = tag;
cache[set_index][min].age = age;
read++; miss++;
}
}else if(input == 'W'){
                int k = 0;
for(int l =0;l<n;l++){
   if(cache[set_index][k].valid != 0 && cache[set_index][k].tag != tag && k<n){
       k = l;
   }
}
            //////////////
    if ((cache[set_index][0].valid == 1)&&(cache[set_index][0].tag == tag)) {
write++; hit++;


}else{
        int min = 0;
k = 0;
 for(int l =0;l<n;l++){
   if(cache[set_index][k].age < cache[set_index][min].age){
       min = k;
   }
}
cache[set_index][min].valid = 1;
cache[set_index][min].tag = tag;
cache[set_index][min].age = age;
read++; write++; miss++;

}
}
age++;
}
printf("Memory reads: %d\nMemory writes: %d\nCache hits: %d\nCache misses: %d\n",read, write,hit,miss);

}

}

void mymainmethodtwo(int argc, char **argv) {

typedef struct CashLine{
int valid;
unsigned long int tag;

unsigned long int age;

}CashLine;
struct CashLine** cache;

if (argc != 6) {
printf("wrong number of inputs\n");
}
    int cacheSize = atoi(argv[1]);
    if(isItPowerTwo(cacheSize) != 1){
        printf("the cashe size is not a power of 2\n");
    }
    char* associativity = argv[2];
    int blockSize = atoi(argv[4]);
       // int cacheSize = atoi(argv[1]);
    if(isItPowerTwo(blockSize) != 1){
        printf("the blocksize is not a power of 2\n");
    }
    FILE *file1 = fopen(argv[5],"r");
         if(associativity[0] == 'd'){

        int setNumbers = cacheSize/blockSize;
        int index_size = logtwo(setNumbers);
        int offset_size = logtwo(blockSize);
        cache=(CashLine**)malloc(setNumbers*sizeof(CashLine*));
      for(int i=0;i<setNumbers;i++){
                cache[i]=(CashLine*)malloc((1)*sizeof(CashLine));
        }
        for(int i=0;i<setNumbers;i++){

            cache[i][0].valid=0;
            cache[i][0].age=0;
            cache[i][0].tag=0;
      }
      int age = 0;

while (!feof(file1)) {
   char pc[99];
char input;
unsigned long int address;
unsigned long int tag;
unsigned long int set_index;
fscanf(file1,"%s %c %lx", pc, &input, &address);
if (pc[0] == '#') {break;}
         long int identification = address >> offset_size;
              set_index = identification & ((1 << index_size) - 1);
               tag = identification >> index_size;



if (input == 'R'){
   if ((cache[set_index][0].valid == 1)&&(cache[set_index][0].tag == tag)) {

hit2++;

}else{
cache[set_index][0].valid = 1;
cache[set_index][0].tag = tag;
cache[set_index][0].age = age;
read2++; miss2++;
//----------------------
   unsigned long int nextaddress = ( address + blockSize) ;
    unsigned long int nexttag;
   unsigned long int nextindex;

//---------------------
    long int nextid = nextaddress >> offset_size;
             nextindex = nextid & ((1 << index_size) - 1);
          nexttag = nextid >> index_size;
               //------------
               if (cache[nextindex][0].tag != nexttag|| cache[nextindex][0].valid !=1) {

  read2++;

cache[nextindex][0].valid = 1;
cache[nextindex][0].tag = nexttag;
cache[nextindex][0].age = age;
// hit2++;

}
}
}else if(input == 'W'){
    if ((cache[set_index][0].valid == 1)&&(cache[set_index][0].tag == tag)) {
write2++; hit2++;


}else{
cache[set_index][0].valid = 1;
cache[set_index][0].tag = tag;
cache[set_index][0].age = age;
read2++; write2++; miss2++;
//----------------------
   unsigned long int nextaddress = ( address + blockSize) ;
    unsigned long int nexttag;
   unsigned long int nextindex;

//---------------------
    long int nextid = nextaddress >> offset_size;
             nextindex = nextid & ((1 << index_size) - 1);
          nexttag = nextid >> index_size;
               //------------
               if (cache[nextindex][0].tag != nexttag|| cache[nextindex][0].valid !=1) {

  read2++;

cache[nextindex][0].valid = 1;
cache[nextindex][0].tag = nexttag;
cache[nextindex][0].age = age;
// hit2++;

}
}
}
age++;
}
printf("Memory reads: %d\nMemory writes: %d\nCache hits: %d\nCache misses: %d\n",read2, write2,hit2,miss2);
}else if(associativity[5] == ':'){
        char c = associativity[5];
        int n = c - '0';
        int setNumbers = cacheSize/blockSize;
        int index_size = logtwo(setNumbers);
        int offset_size = logtwo(blockSize);
        cache=(CashLine**)malloc(setNumbers*sizeof(CashLine*));
      for(int i=0;i<setNumbers;i++){
                cache[i]=(CashLine*)malloc((n)*sizeof(CashLine));
        }
        for(int i=0;i<setNumbers;i++){
         for(int k =0;k<n;k++){
            cache[i][k].valid=0;
            cache[i][k].age=0;
            cache[i][k].tag=0;
         }
      }
      int age = 0;

while (!feof(file1)) {
   char pc[99];
char input;
unsigned long int address;
unsigned long int tag;
unsigned long int set_index;
fscanf(file1,"%s %c %lx", pc, &input, &address);
if (pc[0] == '#') {break;}
         long int identification = address >> offset_size;
              set_index = identification & ((1 << index_size) - 1);
               tag = identification >> index_size;



if (input == 'R'){
          /////////////
              int k = 0;
for(int l =0;l<n;l++){
   if(cache[set_index][k].valid != 0 && cache[set_index][k].tag != tag && k<n){
       k = l;
   }
}
            //////////////

   if ((cache[set_index][k].valid == 1)&&(cache[set_index][k].tag == tag)) {

hit2++;

}else{
    int min = 0;
k = 0;
 for(int l =0;l<n;l++){
   if(cache[set_index][k].age < cache[set_index][min].age){
       min = k;
   }
}
cache[set_index][min].valid = 1;
cache[set_index][min].tag = tag;
cache[set_index][min].age = age;
read2++; miss2++;
//----------------------
   unsigned long int nextaddress = ( address + blockSize) ;
    unsigned long int nexttag;
   unsigned long int nextindex;

//---------------------
    long int nextid = nextaddress >> offset_size;
             nextindex = nextid & ((1 << index_size) - 1);
          nexttag = nextid >> index_size;
               //------------
                            int k2 = 0;
for(int l2 =0;l2<n;l2++){
   if(cache[nextindex][k2].valid != 0 && cache[nextindex][k2].tag != tag && k<n){
       k2 = l2;
   }
}
////////////
               if (cache[nextindex][k2].tag != nexttag|| cache[nextindex][k2].valid !=1) {

  read2++;
int min = 0;
k = 0;
 for(int l =0;l<n;l++){
   if(cache[nextindex][k].age < cache[nextindex][min].age){
       min = k;
   }
}
cache[nextindex][min].valid = 1;
cache[nextindex][min].tag = nexttag;
cache[nextindex][min].age = age;
// hit2++;

}
}
}else if(input == 'W'){
                int k = 0;
for(int l =0;l<n;l++){
   if(cache[set_index][k].valid != 0 && cache[set_index][k].tag != tag && k<n){
       k = l;
   }
}
            //////////////
    if ((cache[set_index][0].valid == 1)&&(cache[set_index][0].tag == tag)) {
write2++; hit2++;


}else{
        int min = 0;
k = 0;
 for(int l =0;l<n;l++){
   if(cache[set_index][k].age < cache[set_index][min].age){
       min = k;
   }
}
cache[set_index][min].valid = 1;
cache[set_index][min].tag = tag;
cache[set_index][min].age = age;
read2++; write2++; miss2++;
//----------------------
   unsigned long int nextaddress = ( address + blockSize) ;
    unsigned long int nexttag;
   unsigned long int nextindex;

//---------------------
    long int nextid = nextaddress >> offset_size;
             nextindex = nextid & ((1 << index_size) - 1);
          nexttag = nextid >> index_size;
               //------------
                            int k2 = 0;
for(int l2 =0;l2<n;l2++){
   if(cache[nextindex][k2].valid != 0 && cache[nextindex][k2].tag != tag && k<n){
       k2 = l2;
   }
}
            //////////////
               if (cache[nextindex][k2].tag != nexttag|| cache[nextindex][k2].valid !=1) {

  read2++;
          int min = 0;
k = 0;
 for(int l =0;l<n;l++){
   if(cache[nextindex][k].age < cache[nextindex][min].age){
       min = k;
   }
}
cache[nextindex][min].valid = 1;
cache[nextindex][min].tag = nexttag;
cache[nextindex][min].age = age;
// hit2++;

}
}
}
age++;
}
printf("Memory reads: %d\nMemory writes: %d\nCache hits: %d\nCache misses: %d\n",read2, write2,hit2,miss2);
}else {

        int n = cacheSize/blockSize;
        int setNumbers = 1;
        int index_size = logtwo(setNumbers);
        int offset_size = logtwo(blockSize);
        cache=(CashLine**)malloc(setNumbers*sizeof(CashLine*));
      for(int i=0;i<setNumbers;i++){
                cache[i]=(CashLine*)malloc((n)*sizeof(CashLine));
        }
        for(int i=0;i<setNumbers;i++){
         for(int k =0;k<n;k++){
            cache[i][k].valid=0;
            cache[i][k].age=0;
            cache[i][k].tag=0;
         }
      }
      int age = 0;

while (!feof(file1)) {
   char pc[99];
char input;
unsigned long int address;
unsigned long int tag;
unsigned long int set_index;
fscanf(file1,"%s %c %lx", pc, &input, &address);
if (pc[0] == '#') {break;}
         long int identification = address >> offset_size;
              set_index = identification & ((1 << index_size) - 1);
               tag = identification >> index_size;



if (input == 'R'){
          /////////////
              int k = 0;
for(int l =0;l<n;l++){
   if(cache[set_index][k].valid != 0 && cache[set_index][k].tag != tag && k<n){
       k = l;
   }
}
            //////////////

   if ((cache[set_index][k].valid == 1)&&(cache[set_index][k].tag == tag)) {

hit2++;

}else{
    int min = 0;
k = 0;
 for(int l =0;l<n;l++){
   if(cache[set_index][k].age < cache[set_index][min].age){
       min = k;
   }
}
cache[set_index][min].valid = 1;
cache[set_index][min].tag = tag;
cache[set_index][min].age = age;
read2++; miss2++;
//----------------------
   unsigned long int nextaddress = ( address + blockSize) ;
    unsigned long int nexttag;
   unsigned long int nextindex;

//---------------------
    long int nextid = nextaddress >> offset_size;
             nextindex = nextid & ((1 << index_size) - 1);
          nexttag = nextid >> index_size;
               //------------
                            int k2 = 0;
for(int l2 =0;l2<n;l2++){
   if(cache[nextindex][k2].valid != 0 && cache[nextindex][k2].tag != tag && k<n){
       k2 = l2;
   }
}
////////////
               if (cache[nextindex][k2].tag != nexttag|| cache[nextindex][k2].valid !=1) {

  read2++;
int min = 0;
k = 0;
 for(int l =0;l<n;l++){
   if(cache[nextindex][k].age < cache[nextindex][min].age){
       min = k;
   }
}
cache[nextindex][min].valid = 1;
cache[nextindex][min].tag = nexttag;
cache[nextindex][min].age = age;
// hit2++;

}
}
}else if(input == 'W'){
                int k = 0;
for(int l =0;l<n;l++){
   if(cache[set_index][k].valid != 0 && cache[set_index][k].tag != tag && k<n){
       k = l;
   }
}
            //////////////
    if ((cache[set_index][0].valid == 1)&&(cache[set_index][0].tag == tag)) {
write2++; hit2++;


}else{
        int min = 0;
k = 0;
 for(int l =0;l<n;l++){
   if(cache[set_index][k].age < cache[set_index][min].age){
       min = k;
   }
}
cache[set_index][min].valid = 1;
cache[set_index][min].tag = tag;
cache[set_index][min].age = age;
read2++; write2++; miss2++;
//----------------------
   unsigned long int nextaddress = ( address + blockSize) ;
    unsigned long int nexttag;
   unsigned long int nextindex;

//---------------------
    long int nextid = nextaddress >> offset_size;
             nextindex = nextid & ((1 << index_size) - 1);
          nexttag = nextid >> index_size;
               //------------
                            int k2 = 0;
for(int l2 =0;l2<n;l2++){
   if(cache[nextindex][k2].valid != 0 && cache[nextindex][k2].tag != tag && k<n){
       k2 = l2;
   }
}
            //////////////
               if (cache[nextindex][k2].tag != nexttag|| cache[nextindex][k2].valid !=1) {

  read2++;
          int min = 0;
k = 0;
 for(int l =0;l<n;l++){
   if(cache[nextindex][k].age < cache[nextindex][min].age){
       min = k;
   }
}
cache[nextindex][min].valid = 1;
cache[nextindex][min].tag = nexttag;
cache[nextindex][min].age = age;
// hit2++;

}
}
}
age++;
}
printf("Memory reads: %d\nMemory writes: %d\nCache hits: %d\nCache misses: %d\n",read2, write2,hit2,miss2);

}

}
